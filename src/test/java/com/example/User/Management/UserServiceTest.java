package com.example.User.Management;

import com.example.User.Management.controller.UserController;
import com.example.User.Management.exception.IncorrectPasswordException;
import com.example.User.Management.exception.UserAlreadyExistsException;
import com.example.User.Management.exception.UserNotFoundException;
import com.example.User.Management.model.LoginRequest;
import com.example.User.Management.model.User;
import com.example.User.Management.repository.LoginUserRepo;
import com.example.User.Management.repository.UserRepo;
import com.example.User.Management.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class UserServiceTest {
    @Mock
    UserRepo userRepo;
    @Mock
    LoginUserRepo loginUserRepo;

    @InjectMocks
    UserService userService;

    private User user;
    @Test
    void registerUser_Success()  {
        User user = new User("def@abc.com","Abc1234@");
        when(userRepo.save(user)).thenReturn(user);
        ResponseEntity<User> responseEntity = userService.registerUser(user);
        log.info("res {}", responseEntity);
        assertEquals(user,responseEntity.getBody());
    }

    @Test
    void registerUser_UserAlreadyExistsException() {
        User user = new User("def@abc.com", "Abc1234@");

        when(userRepo.save(user)).thenThrow(new UserAlreadyExistsException("User already exists!"));

        assertThrows(RuntimeException.class, () -> userService.registerUser(user));
    }

    @Test
    void testIsUserRegistered_WhenUserExists_ReturnsTrue() {
        String username = "abc@abc.com";
        User user = new User("abc@abc.com","Abc1234@");
        when(userRepo.findByUsername(username)).thenReturn(user);

        boolean result = userService.isUserRegistered(username);
        assertTrue(result);
    }

    @Test
    void testIsUserRegistered_WhenUserDoesNotExist_ReturnsFalse() {

        String username = "efwfgwn";
        when(userRepo.findByUsername(username)).thenReturn(null);
        boolean result = userService.isUserRegistered(username);
        assertFalse(result);
    }

    //login
    @Test
    void testLogin_WhenUserExistsAndCorrectPassword_ReturnsLoginSuccessMessage() throws IncorrectPasswordException, UserNotFoundException {

        String username = "abc@abc.com";
        String password = "Abc1234@";
        boolean loggedIn = false;
        LoginRequest loginRequest = new LoginRequest(username, password,loggedIn);
        User user = new User("abc@abc.com","Abc1234@");
        user.setUsername(username);
        user.setPassword(password);
        when(userRepo.findByUsername(username)).thenReturn(user);

        String result = userService.login(loginRequest);

        assertEquals("Login Success!!!", result);
        assertTrue(loginRequest.isLoggedIn());

    }

    @Test
    void testLogin_WhenUserDoesNotExist_ThrowsUserNotFoundException() {

        String username = "xyz@xyz.com";
        String password = "sgdgdh";
        boolean loggedIn = false;
        LoginRequest loginRequest = new LoginRequest(username, password,loggedIn);
        when(userRepo.findByUsername(username)).thenReturn(null);

        assertThrows(UserNotFoundException.class, () -> userService.login(loginRequest));
    }

    @Test
    void testLogin_WhenUserExistsButIncorrectPassword_ThrowsIncorrectPasswordException() {

        String username = "abc@abc.com";
        String password = "sfsgg";
        boolean loggedIn = false;
        LoginRequest loginRequest = new LoginRequest(username, password,loggedIn);
        User user = new User();
        user.setUsername(username);
        user.setPassword("correctPassword"); // Set correct password for the user
        when(userRepo.findByUsername(username)).thenReturn(user);
        assertThrows(IncorrectPasswordException.class, () -> userService.login(loginRequest));
    }


    //logout
    @Test
    public void testLogoutUser_Success() {
        String username = "abc@abc.com";
        LoginRequest loginRequest = new LoginRequest("abc@abc.com","Abc1234@",false);
        loginRequest.setLoggedIn(true);
        Optional<LoginRequest> optionalUser = Optional.of(loginRequest);

        when(loginUserRepo.findByUsername(username)).thenReturn(optionalUser);
        boolean result = userService.logoutUser(username);
        assertTrue(result);
        assertFalse(loginRequest.isLoggedIn());

    }

    @Test
    public void testLogoutUser_UserNotFound() {

        String username = "xyz@xyz.com";
        Optional<LoginRequest> optionalUser = Optional.empty();

        when(loginUserRepo.findByUsername(username)).thenReturn(optionalUser);

        boolean result = userService.logoutUser(username);
        assertFalse(result);
    }


}
