package com.example.User.Management;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserManagementApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void mainMethodStartsSpringApplication() {
		SpringApplication springApplicationMock = Mockito.mock(SpringApplication.class);

		UserManagementApplication.main(new String[0]);

	}

}
