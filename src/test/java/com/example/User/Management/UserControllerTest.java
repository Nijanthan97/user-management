package com.example.User.Management;

import com.example.User.Management.controller.UserController;
import com.example.User.Management.exception.IncorrectPasswordException;
import com.example.User.Management.exception.UserAlreadyExistsException;
import com.example.User.Management.model.LoginRequest;
import com.example.User.Management.model.User;
import com.example.User.Management.repository.LoginUserRepo;
import com.example.User.Management.repository.UserRepo;
import com.example.User.Management.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {
    @Mock
    UserRepo userRepo;
    @Mock
    LoginUserRepo loginUserRepo;
    @Mock
    UserService userService;
    @InjectMocks
    UserController userController;

    @Test
    public void createUser_ValidUser_Success() throws UserAlreadyExistsException {

        User user = new User("abc@abc.com","Abc1234@");
        user.setUsername("abc@abc.com");
        user.setPassword("Abc1234@");

        when(userService.registerUser(any(User.class))).thenReturn(new ResponseEntity<>(user, HttpStatus.CREATED));


        ResponseEntity<User> response = userController.createUser(user);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        assertEquals(user, response.getBody());
    }

    @Test
    public void createUser_UserAlreadyExists_ExceptionThrown() throws UserAlreadyExistsException {

        User user = new User("abc@abc.com", "Abc1234@");
        user.setUsername("abc@abc.com");
        user.setPassword("Abc1234@");

        when(userService.registerUser(any(User.class))).thenThrow(new UserAlreadyExistsException("User already exists"));
        assertThrows(RuntimeException.class, () -> userController.createUser(user));

    }

    @Test
    public void login_ValidCredentials_Success() throws IncorrectPasswordException {

        LoginRequest loginRequest = new LoginRequest("abc@abc.com","Abc1234@",false);
        loginRequest.setUsername("abc@abc.com");
        loginRequest.setPassword("Abc1234");

        when(userService.login(any(LoginRequest.class))).thenReturn("success");

        String response = userController.login(loginRequest);

        assertEquals("success", response);
    }

    @Test
    public void login_IncorrectPassword_ExceptionThrown() throws IncorrectPasswordException {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("testUser");
        loginRequest.setPassword("incorrectPassword");
        when(userService.login(any(LoginRequest.class))).thenThrow(new IncorrectPasswordException("Incorrect password"));
        assertThrows(IncorrectPasswordException.class, () -> userController.login(loginRequest));

    }

    @Test
    public void logoutUser_ValidUsername_Success() {
        String username = "abc@abc.com";
        when(userService.logoutUser(username)).thenReturn(true);
        boolean response = userController.logoutUser(username);
        assertEquals(true, response);
    }

    @Test
    public void logoutUser_InvalidUsername_Failure() {
        String username = "xyz@xyz.com";

        when(userService.logoutUser(username)).thenReturn(false);

        boolean response = userController.logoutUser(username);

        assertEquals(false, response);
    }

}
