package com.example.User.Management.controller;

import com.example.User.Management.exception.IncorrectPasswordException;
import com.example.User.Management.exception.UserAlreadyExistsException;
import com.example.User.Management.exception.UserNotFoundException;
import com.example.User.Management.model.LoginRequest;
import com.example.User.Management.model.User;
import com.example.User.Management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/register")
    public ResponseEntity<User> createUser(@RequestBody @Valid User user) throws UserAlreadyExistsException {

        return userService.registerUser(user);
    }

    @PostMapping("/login")
    public String login(@RequestBody @Valid LoginRequest loginRequest) throws IncorrectPasswordException, UserNotFoundException {
        return userService.login(loginRequest);
    }
    @PostMapping("/logout/{username}")
    public boolean logoutUser(@PathVariable String username){
        return userService.logoutUser(username);
    }






}
