package com.example.User.Management.service;

import com.example.User.Management.exception.IncorrectPasswordException;
import com.example.User.Management.exception.UserAlreadyExistsException;
import com.example.User.Management.exception.UserNotFoundException;
import com.example.User.Management.model.LoginRequest;
import com.example.User.Management.model.User;
import com.example.User.Management.repository.LoginUserRepo;
import com.example.User.Management.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private LoginUserRepo loginUserRepo;

    public ResponseEntity<User> registerUser(User user) throws UserAlreadyExistsException {
        String username = user.getUsername();
        if (isUserRegistered(username)) {
            throw new UserAlreadyExistsException("Username already exists. Please choose a different username.");
        } else {
            User saveUser = userRepo
                    .save(user);
            return new ResponseEntity<>(saveUser, HttpStatus.CREATED);
        }
    }
        public String login(LoginRequest loginRequest ) throws IncorrectPasswordException {
        User user = userRepo.findByUsername(loginRequest.getUsername());
        if (user == null) {
            throw new UserNotFoundException("User not found");
        }
        if (isUserRegistered(user.getUsername())){
            if (loginRequest.getPassword().equals(user.getPassword())) {
                loginRequest.setLoggedIn(true);
                loginUserRepo.save(loginRequest);
                return "Login Success!!!";
            }
            }
            throw new IncorrectPasswordException("Incorrect Password");

    }

    public boolean isUserRegistered(String username) {
        User user = userRepo.findByUsername(username);
        return user != null;
    }

    public boolean logoutUser(String username) {
            Optional<LoginRequest> optionalUser = loginUserRepo.findByUsername(username);
            if (optionalUser.isPresent()) {
                LoginRequest loginRequest = optionalUser.get();
                loginRequest.setLoggedIn(false);
                loginUserRepo.save(loginRequest);
                return true;
            } else {
                return false;
            }

    }



}
