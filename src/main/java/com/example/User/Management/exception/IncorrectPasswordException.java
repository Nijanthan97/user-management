package com.example.User.Management.exception;

public class IncorrectPasswordException extends Exception {
        public IncorrectPasswordException(String password) {
            super("Incorrect password");
        }
}
