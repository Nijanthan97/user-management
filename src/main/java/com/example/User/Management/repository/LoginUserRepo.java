package com.example.User.Management.repository;

import com.example.User.Management.model.LoginRequest;
import com.example.User.Management.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginUserRepo extends JpaRepository<LoginRequest,String> {
    Optional<LoginRequest> findByUsername(String username);
}
