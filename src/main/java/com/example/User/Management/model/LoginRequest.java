package com.example.User.Management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "loginuser")
public class LoginRequest {
    @Id
    @NotBlank(message = "User name Should not be blank")
    @Email
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$", message = "Invalid email format")
    private String username;
    @NotBlank(message = "Password should not be blank")
    private String password;

    private boolean LoggedIn;
}
