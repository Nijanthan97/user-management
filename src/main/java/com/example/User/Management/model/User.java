package com.example.User.Management.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class User {
    @Id
    @Column(name = "username")
    @NotNull(message = "User name Should not be null")
    @NotBlank(message = "User name Should not be blank")
    @Email
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$", message = "Invalid email format")
    private String username;

    @Column(name = "password")
    @NotNull(message = "Password Should not be null")
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",message = "Enter a valid password")
    @NotBlank(message = "Password should not be blank")
    private String password;

}
